# LunarPower

Increase player stats depending on your total lunar coins, configurable.

## Installation

1. First install BepInEx and ModRecalculate

BepInEx : https://thunderstore.io/package/bbepis/BepInExPack/

ModRecalculate : https://thunderstore.io/package/PlexusDuMenton/ModRecalculate/

2. Extract the content of the archive into Risk of Rain 2\BepInEx\plugins\

## Configuration

Launch the game once to generate the configuration file located at Risk of Rain 2\BepInEx\config\com.ponywarrior.lunarpower.cfg

## Credits

bbepis for making BepInEx

PlexusDuMenton for making ModRecalculate and helping me use it

## Changelog

v1.0.0 Initial Release