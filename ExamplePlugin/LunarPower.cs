﻿using BepInEx;
using BepInEx.Configuration;
using RoR2;
using PlexusUtils;

namespace LunarPower
{
    [BepInDependency("com.Plexus.ModRecalculate")]
    [BepInPlugin("com.ponywarrior.lunarpower", "LunarPower", "1.0.0")]
    public class LunarPower : BaseUnityPlugin
    {
        public static ConfigWrapper<int> Percentage { get; private set; }
        public static ConfigWrapper<int> CoinLimit { get; private set; }
        private LocalUser user;
        private float bonus;
        private void Awake()
        {
            SetConfigWraps();
            ModRecalculate.HealthRecalculation += RecalcHealth;
            ModRecalculate.DamageRecalculation += RecalcDamage;
            ModRecalculate.RegenRecalculation += RecalcRegen;
            Chat.AddMessage($"Lunar Power mod working! Current setting : {Percentage.Value}% stats per coin, {CoinLimit.Value} coins limit!");
        }
        private void SetConfigWraps()
        {
            int defaultPercentage = 5;
            Percentage = Config.Wrap("Percentages","PercentagePerCoin", "Increase stats by x% for each lunar coin you have. Integral numbers only.", defaultPercentage);
            int defaultCoinLimit = 0;
            CoinLimit = Config.Wrap("Limits", "CoinLimit", "Set a limit to how many coins give stats. 0 for no limit.", defaultCoinLimit);
        }
        private float BonusCalc(float stat)
        {
            if (bonus == 0f || bonus != user.userProfile.coins)
            {
                bonus = user.userProfile.coins;
                if (CoinLimit.Value > 0f && bonus > CoinLimit.Value)
                {
                    bonus = CoinLimit.Value;
                }
            }
            return stat * (bonus / (100f / Percentage.Value));
        }
        float RecalcHealth(CharacterBody character)
        {
            if (user == null)
            {
                user = LocalUserManager.GetFirstLocalUser();
            }
            return BonusCalc(character.baseMaxHealth);
        }
        float RecalcDamage(CharacterBody character)
        {
            if (user == null)
            {
                user = LocalUserManager.GetFirstLocalUser();
            }
            return BonusCalc(character.baseDamage);
        }
        float RecalcRegen(CharacterBody character)
        {
            if (user == null)
            {
                user = LocalUserManager.GetFirstLocalUser();
            }
            return BonusCalc(character.baseRegen);
        }
    }
}